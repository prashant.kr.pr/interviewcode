//Write a function to find out longest palindrome in a given string?

const is_palindrom = (palinString) => {
    let re = /[\W_]/g;
    let removeSpecialChar = palinString.replace(re, '');
    let reverseString = removeSpecialChar.split('').reverse().join('');
    return reverseString === removeSpecialChar;
}

const longestPalinString = (testString) => {
    let stringLength = testString.length;
    if (stringLength === 0)
        return 'Empty String.'

    var longestString = '';
    var longestStringLength = 0;

    for (let i = 0; i < stringLength; i++) {
        var subString = testString.substr(i, stringLength);

        for(let j=subString.length;j>=0;j--) {
            let sub_sub_string = subString.substr(0,j);
            if(sub_sub_string.length <=1) {
                continue;
            }
            if(is_palindrom(sub_sub_string) && longestStringLength < sub_sub_string.length) {
                longestStringLength = sub_sub_string.length;
                longestString = sub_sub_string;
            }
        }
    }
    return longestString;

}

console.log(longestPalinString('abracadabra'));