// find the missing number in an array

const findMissingNumber = (testArray) => {
    var sortedArray = testArray.sort();
    for (let i = 1; i < sortedArray.length; i++) {
        if (sortedArray[i] - sortedArray[i - 1] > 1) {
            return sortedArray[i] - 1;
        }
    }

}


console.log(findMissingNumber([1, 2, 3, 5, 8, 7, 6]));