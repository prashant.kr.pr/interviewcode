// Three Ways to Reverse a String in JavaScript

function reverseString1(str) {
    return str.split("").reverse().join("");
}

function reverseString2(str) {
    var newString = "";
    for (var i = str.length - 1; i >= 0; i--) {
        newString += str[i];
    }
    return newString;
}

function reverseString(str) {
    if (str === "")
      return "";
    else
      return reverseString(str.substr(1)) + str.charAt(0);
  }
  var testDemo = "abcde";
  console.log(testDemo);
  console.log(testDemo.substr(2));
//   console.log(reverseString("hello"));