class User {
    constructor(firstname, lastname, credit) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.credit = credit;
    }

    getFullName() {
        return `${this.firstname} ${this.lastname} is my full name.`;
    }
}

const jhon = new User('Jhon', 'Anderson', 34);
console.log(jhon)
console.log(jhon.getFullName())

const shammy = new User();