function foo() {
    let a = b = 0;
    a++;
    return a;
  }
  
  foo();
  console.log(typeof a); // => ???
  console.log(typeof b); // => ???
  
//   const clothes = ['jacket', 't-shirt'];
//   clothes.length = 0;
  
//   clothes[0]; // => ???
//   Input
//   =========
//   const players = [
//   {name:” Tom”, score:[12,34, 80]},
//   {name:”Faisal”, score:[2,30, 86]},
//   {name:”Anjith”, score:[7,94, 9]},
//   ]
//   Out put
//   ================
//   Anjith : 94
//   Faisal : 86
//   Tom  : 80

//   Let a
//   Let b
  