
const duplicateArray = [1, 1, 2, 3, 4, 3, 2, 1];
getUnique(duplicateArray);

// Defining function to get unique values from an array
function getUnique(array) {
    var uniqueArray = [];

    // Loop through array values
    for (var value of array) {
        if (uniqueArray.indexOf(value) === -1) {
            uniqueArray.push(value);
        }
    }
    return uniqueArray;
}

var names = ["John", "Peter", "Clark", "Harry", "John", "Alice"];
var uniqueNames = getUnique(duplicateArray);
console.log(uniqueNames); // Prints: ["John", "Peter", "Clark", "Harry", "Alice"]