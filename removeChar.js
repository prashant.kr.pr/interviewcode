//Write a method which will remove any given character from a String?

var testString = "abcde f_gh";
var toRemove = "b";

const removeString = (testString, toRemove) => {
    const finalStr = testString.split('').filter((ele) => ele !== toRemove);
    console.log(finalStr.join(''))
}

removeString(testString, toRemove);