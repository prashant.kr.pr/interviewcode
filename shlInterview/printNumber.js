/**
 * print from 1 to 100
 * If multipe of 3 then print "buzz" and if multiple of 5 then print "fizz"
 * and if multiple of 3 and 5 then print "fizzbuzz"
 */

 const isThreeMultiple = (num) => num%3 === 0
 const isFiveMultiple = (num) => num%5 === 0

 const printNumber = () => {
     for(let i=1;i<=100;i++) {
        if(isThreeMultiple(i) && isFiveMultiple(i)) {
            console.log("fizzbuzz")
        } else if(isThreeMultiple(i)) {
            console.log("buzz")
        } else if(isFiveMultiple(i)) {
            console.log("fizz")
        } else {
            console.log(i)
        }
     }
 }
 printNumber();