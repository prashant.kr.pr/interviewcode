// Question 1
/**
 * The output will be 
 * 1
 * 4
 * 3 after pause of 1sec it will print 2.
 * 2
 */
/** 
(function(){
    console.log(1);
    setTimeout(function(){console.log(2)},1000)
    setTimeout(function(){console.log(3)},0)
    console.log(4)
})()
*/

//Question 2
/**
 * Here the trick is that for the external clog(a) the value of var will not work. So it will throw
 * error. If we comment the last clog(a) then the answer will be 
 * undefined
 * 10
 * The scope of "var a=10;" is only limited to the function.
 */
/** 
function show() {
    console.log(a);
    var a = 10;
    console.log(a);
}

show();
// console.log(a)
*/

// Question 3
/**
 * Ref:- https://www.youtube.com/watch?v=-xqJo5VRP4A
 * This question involves the concept of closure and scope
 * 
 * Here the trick is that the scope of var is functional so when for first time loop runs then it will 
 * go to setTimeout but their is delay of 1 second so it will not print anything is that time but
 * the loop will go on. So when the time comes to print then up till that time the value of i becomes
 * 5 so it will print 5 "5 times."
 */
/** 
 for(var i=0;i<5;i++) {
    console.log(i,'--')
    setTimeout(() => { console.log(i) }, i*1000)
}
*/
/**
 * Since this is let so for each iteration/loop there will be new "i" so the closure will hold the old
 * value.
 * It will print 0,1,2,3,4
 */
/** 
 for(let i=0;i<5;i++) {
    console.log(i,'--')
    setTimeout(() => { console.log(i) }, i*1000)
}
*/

// Question 4
/** 
let f = (...f) => f;
console.log(f(10)); //this will print 10

f = (...f) => reduce(f => f) //this will throw error.
console.log(f(10));
*/

//Question 5
/**
 * Ans 1
 * undefined
 * 2
 */
/** 
(() => {
    let x,y;
    try{
        throw new Error();
    } catch(x) {
        (x=1),(y=2);
        console.log(x);
    }
    console.log(x);
    console.log(y);
})()
*/

//Question 6
// /** 
class Dog {
    constructor(name) {
        this.name = name
    }
}

Dog.prototype.bark = function(){
    console.log(`Woof I am ${this.name}`)
}

const pet1 = new Dog('Dog1')
pet1.bark();    // this will print the value.
 delete Dog.prototype.bark;
 const pet2 = new Dog('Dog2')

pet1.bark();    // This will show error. as the prototype os deleted.
pet2.bark();    // This will show error.

// */

// Question 7
/**
 * Out will be {a:'three',b:'two'}
 */
/** 
const obj = {a:"one",b:"two",a:"three"};
console.log(obj)
*/

// Question 8
// console.log(typeof typeof 1)