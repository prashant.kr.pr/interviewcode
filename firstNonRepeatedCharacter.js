// find first non repeated character.

const findFirstNonRepeatedChar = (testString) => {
    var ctr = 0;
    var result='';
    for(let i=0;i<testString.length;i++) {
        ctr=0;
        for(let j=0;j<testString.length;j++){
            if(testString[i] === testString[j]) {
                ctr+=1;
            }
        }
        if(ctr<2) {
            result=testString[i];
            break;
        }
    }
    return result;
}

console.log(findFirstNonRepeatedChar('abcdabc'))